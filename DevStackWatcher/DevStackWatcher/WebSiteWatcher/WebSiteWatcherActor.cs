﻿using System;
using System.Net.Http;
using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using System.Threading.Tasks;
using DevStackWatcher.Database;
using System.Diagnostics;

namespace DevStackWatcher.WebSiteWatcher
{
    class WebSiteWatcherActor : ReceiveActor
    {
        #region Fields
        /// <summary>
        /// The Akka.Net logger
        /// </summary>
        private readonly ILoggingAdapter _log = Context.GetLogger<SerilogLoggingAdapter>();
        #endregion

        #region Properties
        /// <summary>
        /// The url of the watching website.
        /// </summary>
        public string WebSiteUrl { get; private set; }

        /// <summary>
        /// The content of the watching website landing page.
        /// </summary>
        public string WebSiteContent { get; private set; }

        /// <summary>
        /// Scheduler Time for checking time.
        /// </summary>
        public int WebSiteCheckingTimeIntervall { get; private set; }

        public HttpClient httpClient { get; private set; }

        // better implement List of IActorRefs to handle multiple subscriber
        public IActorRef WebSiteWatcherDataSubscriber { get; private set; }

        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public WebSiteWatcherActor(string webSiteUrl, string webSiteContent)
        {
            _log.Debug("Constructor");

            this.WebSiteUrl = webSiteUrl;
            this.WebSiteContent = webSiteContent;
            this.WebSiteCheckingTimeIntervall = 10000; //ms
            this.httpClient = new HttpClient();

            // Setup Messages
            SetupMessages();

            // Tell Parent to create db writer actor
            Context.Parent.Tell(new AddDbWriterActor(this.WebSiteUrl));
        }

        #region Messages Handling
        private void SetupMessages()
        {
            Receive<AddWebSiteWatcherActorMsg>(message => this.AddWebSiteWatcherActorMsgHandler(message));
            Receive<StartWebSiteWatcherActorMsg>(message => this.StartWebSiteWatcherActorMsgHandler(message));
            Receive<StopWebSiteWatcherActorMsg>(message => this.StopWebSiteWatcherActorMsgHandler(message));
            Receive<CheckWebSiteAvailabilityMsg>(message => this.CheckWebSiteAvailabilityMsgHandler(message));
            Receive<SubscribeWebSiteWatcherDataMsg>(message => this.SubscribeWebSiteWatcherDataMsgHandler(message));
            //Receive<HttpResponseMessage>(message => this.HttpResponseMessageHandler(message));
            //Receive<string>(message => this.WebSiteContentMsgHandler(message));
        }

        private void SubscribeWebSiteWatcherDataMsgHandler(SubscribeWebSiteWatcherDataMsg message)
        {
            // save subscriber
            WebSiteWatcherDataSubscriber = Context.Sender;
        }

        /// <summary>
        /// Message Handler for CheckWebSiteAvailabilityMsg
        /// Check the Web Site Availability by getting Status Code and Web Site Content
        /// </summary>
        /// <param name="message"></param>
        private void CheckWebSiteAvailabilityMsgHandler(CheckWebSiteAvailabilityMsg message)
        {
            // Initialize variables
            Task<HttpResponseMessage> taskHttpResponseCode = null;
            Task<string> taskWebSiteContent = null;
            string httpResponseCode = "-1";
            bool contentFound = false;
            TimeSpan ResponseDuration = new TimeSpan(0, 0, 0, 0, 0);

            try
            {
                // Start task to get Status Code and Web Site Content
                taskHttpResponseCode = httpClient.GetAsync(this.WebSiteUrl);
                taskWebSiteContent = httpClient.GetStringAsync(this.WebSiteUrl);
                Task[] taskArr = new Task[] { taskHttpResponseCode, taskWebSiteContent };
                Stopwatch stopwatch = Stopwatch.StartNew();

                // Waiting for all tasks to finish causing blocking of Actor
                Task.WaitAll(taskArr, 2000);
                stopwatch.Stop();
                //_log.Debug(stopwatch.ElapsedMilliseconds.ToString());
                ResponseDuration = stopwatch.Elapsed;
            }
            catch (Exception ex)
            {
                _log.Warning(ex.Message);
            }

            // If Error while waiting for tasks set "httpresponsecode"=-1
            if(taskHttpResponseCode != null)
            {   // Convert Status Code
                httpResponseCode = ((int)taskHttpResponseCode.Result.StatusCode).ToString();
                _log.Debug(httpResponseCode);
            }
            else
            {
                _log.Debug(httpResponseCode);
            }

            // Check if retrieved Web Site Content contains the given Web Site Content 
            if(taskWebSiteContent != null)
            {
                if (taskWebSiteContent.Result.Contains(this.WebSiteContent))
                {
                    _log.Debug("Content found!");
                    contentFound = true;
                }
                else
                {
                    _log.Debug("Content NOT found! Instead found: " + taskWebSiteContent.Result);
                }
            }

            // Check if someone has already subscribed the data
            if (WebSiteWatcherDataSubscriber != null)
            {
                WebSiteWatcherDataSubscriber.Tell(new AsyncRequestResponseMsg(contentFound, this.WebSiteUrl, httpResponseCode, DateTime.Now, ResponseDuration));
            }

            // Use PipeTo pattern to avoid blocking of actor
            //this.GetHttpRequestAsync(this.WebSiteUrl).PipeTo(Self);
            //this.GetWebSiteContentAsync(this.WebSiteUrl).PipeTo(Self);
        }

        /*
        /// <summary>
        /// Get HTTP Request asynchronous.
        /// </summary>
        /// <param name="ipAddress"></param>
        public Task<bool> GetHttpRequestAsync(string webSiteUrl)
        {
            Task<HttpResponseMessage> a = httpClient.GetAsync(this.WebSiteUrl);
            Task <string> b = httpClient.GetStringAsync(this.WebSiteUrl);

            Task.WaitAll(a, b);

            bool contentFound = false;         
            if (b.Result.Contains(this.WebSiteContent))
            {
                _log.Debug("Content found!");
                contentFound = true;
            }

            string statusCode = ((int)a.Result.StatusCode).ToString();

            var retVal = new AsyncRequestResponseMsg(contentFound, statusCode, b.Result, DateTime.Now);

            return new Task<bool>;
        }
        */

        /*
        /// <summary>
        /// Get Web Site content asynchronous.
        /// </summary>
        /// <param name="ipAddress"></param>
        public Task<string> GetWebSiteContentAsync(string webSiteUrl)
        {
            return httpClient.GetStringAsync(this.WebSiteUrl);
        }
        */

        private void StopWebSiteWatcherActorMsgHandler(StopWebSiteWatcherActorMsg message)
        {
            
        }

        private void StartWebSiteWatcherActorMsgHandler(StartWebSiteWatcherActorMsg message)
        {
            Context.System.Scheduler.ScheduleTellRepeatedly(0, this.WebSiteCheckingTimeIntervall, Self, new CheckWebSiteAvailabilityMsg(), Self);
        }

        private void AddWebSiteWatcherActorMsgHandler(AddWebSiteWatcherActorMsg message)
        {
            _log.Info("WebSiteWatcherActor created for website: {0}", this.WebSiteUrl);
        }
        #endregion

        public static Props Props(string webSiteUrl, string webSiteContent)
        {
            return Akka.Actor.Props.Create(() => new WebSiteWatcherActor(webSiteUrl, webSiteContent));
        }
    }
}
