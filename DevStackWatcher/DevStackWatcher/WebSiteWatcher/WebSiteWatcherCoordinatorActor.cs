﻿using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using DevStackWatcher.Database;
using System;
using System.Collections.Generic;

namespace DevStackWatcher.WebSiteWatcher
{
    /// <summary>
    /// Coordination Actor for all WebSiteWatcher Actors
    /// </summary>
    class WebSiteWatcherCoordinatorActor : ReceiveActor
    {
        #region Fields
        /// <summary>
        /// The Akka.Net logger
        /// </summary>
        private readonly ILoggingAdapter _log = Context.GetLogger<SerilogLoggingAdapter>();
        #endregion

        #region Properties
        /// <summary>
        /// List of child actors.
        /// </summary>
        public List<IActorRef> lstChildActors { get; private set; }

        public IActorRef DbCoordinatorActor { get; private set; }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public WebSiteWatcherCoordinatorActor(IActorRef dbCoordinatorActor)
        {
            _log.Debug("Constructor");

            lstChildActors = new List<IActorRef>();
            DbCoordinatorActor = dbCoordinatorActor;

            // Setup messages
            SetupMessages();
        }

        #region Message Handling

        /// <summary>
        /// Setup Messages
        /// </summary>
        private void SetupMessages()
        {
            Receive<AddWebSiteWatcherActorMsg>(message => this.AddWebSiteWatcherActorMsgHandler(message));
            Receive<StartAllWebSiteWatcherActorMsg>(message => this.StartAllWebSiteWatcherActorMsgHandler(message));
            Receive<StopAllWebSiteWatcherActorMsg>(message => this.StopAllWebSiteWatcherActorMsgHandler(message));
            Receive<AddDbWriterActor>(message => this.AddDbWriterActorMsgHandler(message));
        }

        private void AddDbWriterActorMsgHandler(AddDbWriterActor message)
        {
            // Tell File Db Coordinator Actor to add new db writer actor
            DbCoordinatorActor.Forward(message);
        }

        private void StopAllWebSiteWatcherActorMsgHandler(StopAllWebSiteWatcherActorMsg message)
        {
            foreach (var childActor in lstChildActors)
            {
                childActor.Tell(new StopWebSiteWatcherActorMsg());
            }
        }

        private void StartAllWebSiteWatcherActorMsgHandler(StartAllWebSiteWatcherActorMsg message)
        {
            foreach (var childActor in lstChildActors)
            {
                childActor.Tell(new StartWebSiteWatcherActorMsg());
            }
        }

        private void AddWebSiteWatcherActorMsgHandler(AddWebSiteWatcherActorMsg message)
        {
            bool exists = false;
            /*
            // check if given actor for a specific website alreade exists
            foreach (IActorRef childActor in lstChildActors)
            {
                // estimating all actors in lst are of type WebSiteWatcherActor
                WebSiteWatcherActor actor = (WebSiteWatcherActor)childActor;
                if(actor.WebSiteUrl == message.WebSiteUrl)
                {
                    exists = true;
                }
            }
            */

            if(exists == false)
            {
                var actorNameTemp = message.WebSiteUrl.Replace(".", "");
                var actorNameTemp2 = actorNameTemp.Replace("/", "");
                var actorName = actorNameTemp2.Replace(":", "");

                IActorRef actorRef = Context.ActorOf(WebSiteWatcherActor.Props(message.WebSiteUrl, message.WebSiteContent), actorName);
                lstChildActors.Add(actorRef);
                actorRef.Forward(message);
            }
            
        }

        #endregion


        // Actor Lifecycle

        /// <summary>
        /// Pre Start method
        /// </summary>
        protected override void PreStart()
        {
            base.PreStart();
            _log.Debug("Starts WebSiteWatcherCoordinatorActor");
        }

        /// <summary>
        /// Post Stop method
        /// </summary>
        protected override void PostStop()
        {
            base.PostStop();
            _log.Debug("Stops WebSiteWatcherCoordinatorActor");
        }
    }

    #region Messages
    // // better Implement all Messages and enums in Base / Infrastructure

    /// <summary>
    /// This class represents a message to add a web site watching actor.
    /// </summary>
    public class AddWebSiteWatcherActorMsg
    {
        public string WebSiteUrl { get; private set; }
        public string WebSiteContent { get; private set; }

        public AddWebSiteWatcherActorMsg(string webSiteUrl, string webSiteContent)
        {
            WebSiteUrl = webSiteUrl;
            WebSiteContent = webSiteContent;
        }
    }

    /// <summary>
    /// This class represents a message to start all web site watching actors.
    /// </summary>
    public class StartAllWebSiteWatcherActorMsg
    { }

    /// <summary>
    /// This class represents a message to stop all web site watching actors.
    /// </summary>
    public class StopAllWebSiteWatcherActorMsg
    { }

    /// <summary>
    /// This class represents a message to start one web site watching actors.
    /// </summary>
    public class StartWebSiteWatcherActorMsg
    { }

    /// <summary>
    /// This class represents a message to stop one web site watching actors.
    /// </summary>
    public class StopWebSiteWatcherActorMsg
    { }

    /// <summary>
    /// This class represents a message to check the we site availability .
    /// </summary>
    public class CheckWebSiteAvailabilityMsg
    { }

    public class AsyncRequestResponseMsg
    {
        public bool OverallStatus { get; private set; }
        public string WebSiteUrl { get; private set; }
        public string StatusCode { get; private set; }
        public DateTime ResponseTimeStamp { get; private set; }
        public TimeSpan ResponseDuration { get; private set; }

        public AsyncRequestResponseMsg(bool overallStatus, string webSiteUrl, string statusCode, DateTime responseTime, TimeSpan responseDuration)
        {
            OverallStatus = overallStatus;            
            WebSiteUrl = webSiteUrl;
            StatusCode = statusCode;
            ResponseTimeStamp = responseTime;
            ResponseDuration = responseDuration;
        }
    }


    #endregion
}
