﻿using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using DevStackWatcher.DevStackApi.Models;
using DevStackWatcher.WebSiteWatcher;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;


namespace DevStackWatcher.Database
{
    class DbWriterActor : ReceiveActor
    {
        #region Fields
        /// <summary>
        /// The Akka.Net logger
        /// </summary>
        private readonly ILoggingAdapter _log = Context.GetLogger<SerilogLoggingAdapter>();
        #endregion

        #region Properties
        /// <summary>
        /// The url of the watching website.
        /// </summary>
        public string HttpUrl { get; private set; }

        /// <summary>
        /// ActorRef of Data Publishing Actor
        /// </summary>
        public IActorRef WebSiteWatcherDataPublisher { get; private set; }

        /// <summary>
        /// HttpClient for Accessing the DataBase via WebAPI
        /// </summary>
        public HttpClient httpClient { get; private set; }

        internal static Props Props(string webSiteUrl)
        {
            return Akka.Actor.Props.Create(() => new DbWriterActor(webSiteUrl));
        }
        #endregion

        public DbWriterActor(string httpUrl)
        {
            _log.Info("Constructor");
            HttpUrl = httpUrl;
            this.httpClient = new HttpClient();

            // Setup Messages
            SetupMessages();
        }

        #region Message Handling
        private void SetupMessages()
        {
            Receive<AddDbWriterActor>(message => this.AddDbWritingActorMsgHandler(message));
            Receive<AsyncRequestResponseMsg>(message => this.AsyncRequestResponseMsgHandler(message));
            Receive<HttpResponseMessage>(message => this.HttpResponseMessageHandler(message));
        }

        private void HttpResponseMessageHandler(HttpResponseMessage message)
        {
            _log.Debug(message.ReasonPhrase);
        }

        private void AsyncRequestResponseMsgHandler(AsyncRequestResponseMsg message)
        {
            var httpResponse = new HttpResponse
            {
                TimeStamp = message.ResponseTimeStamp,
                ResponseCode = message.StatusCode,
                RequestedUrl = message.WebSiteUrl,
                OverallStatus = message.OverallStatus.ToString(),
                RequestDurationMilliSeconds = message.ResponseDuration.TotalMilliseconds.ToString()
            };

            // Serialize Object to JSON String
            string jsonHttpResponse = JsonSerializer.Serialize(httpResponse);

            // POST Element: HttpResponse
            this.HttpClientPostAsync(jsonHttpResponse).PipeTo(Self);
        }

        public Task<HttpResponseMessage> HttpClientPostAsync(string jsonInString)
        {
            return this.httpClient.PostAsync("https://localhost:44317/api/httpresponses/"
                , new StringContent(jsonInString, Encoding.UTF8, "application/json"));
        }

        private void AddDbWritingActorMsgHandler(AddDbWriterActor message)
        {
            WebSiteWatcherDataPublisher = Context.Sender;
            WebSiteWatcherDataPublisher.Tell(new SubscribeWebSiteWatcherDataMsg());
        }
        #endregion
    }
}
