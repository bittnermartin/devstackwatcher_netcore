﻿using Akka.Actor;
using Akka.Event;
using Akka.Logger.Serilog;
using System;
using System.Collections.Generic;

namespace DevStackWatcher.Database
{
    class DbCoordinatorActor : ReceiveActor
    {
        #region Fields
        /// <summary>
        /// The Akka.Net logger
        /// </summary>
        private readonly ILoggingAdapter _log = Context.GetLogger<SerilogLoggingAdapter>();
        #endregion

        #region Properties
        /// <summary>
        /// List of child actors.
        /// </summary>
        public List<IActorRef> lstChildActors { get; private set; }

        #endregion  

        public DbCoordinatorActor()
        {
            _log.Debug("Constructor");

            lstChildActors = new List<IActorRef>();

            // Setup Messages
            SetupMessages();
        }

        #region Message Handling
        private void SetupMessages()
        {
            Receive<AddDbWriterActor>(message => this.AddDbWriterActorMsgHandler(message));
        }

        private void AddDbWriterActorMsgHandler(AddDbWriterActor message)
        {
            _log.Debug("Create DbWritingActor for website : " + message.WebSiteUrl);
            // Create DbWritingActor and Forward Message
            IActorRef actorRef = Context.ActorOf(DbWriterActor.Props(message.WebSiteUrl));
            lstChildActors.Add(actorRef);
            actorRef.Forward(message);
        }
        #endregion
    }

    #region Messages
    // better Implement all Messages and enums in Base / Infrastructure

    public class AddDbWriterActor
    {
        public string WebSiteUrl { get; private set; }

        public AddDbWriterActor(string httpUrl)
        {
            WebSiteUrl = httpUrl;
        }
    }

    public class SubscribeWebSiteWatcherDataMsg
    {
        public SubscribeWebSiteWatcherDataMsg()
        {

        }
    }

    #endregion
}
