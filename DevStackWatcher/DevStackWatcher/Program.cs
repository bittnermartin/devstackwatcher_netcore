﻿using System;
using System.Collections.Generic;
using Akka.Actor;
using DevStackWatcher.Database;
using DevStackWatcher.WebSiteWatcher;
using Serilog;

namespace DevStackWatcher
{
    class Program
    {
        static void Main(string[] args)
        {
            //ToDo: Use of Dependency Injection
            //ToDo: Introduce Project with Base / Infrastructure Files

            //ToDo: Try Catch Arround HTTP Request to get additional infos

            // setup system logging 
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console(outputTemplate: "{Timestamp:HH:mm:ss.fff zzz} [{Level:u3}] [{SourceContext}] {Message}{NewLine}{Exception}")
                .WriteTo.RollingFile(AppDomain.CurrentDomain.BaseDirectory + "\\Logs\\DevStackWatcher-{Date}.log"
                    , retainedFileCountLimit: 7
                    , outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level:u3}] [{SourceContext}] {Message}{NewLine}{Exception}")
                .CreateLogger();

            // initialize actor system
            Log.Information("Intializing Actor System");
            var actsys = ActorSystem.Create("actory-system-devstackwatcher", "akka {loglevel=DEBUG,  loggers=[\"Akka.Logger.Serilog.SerilogLogger, Akka.Logger.Serilog\"]}");

            // initialize top level actors
            //var fwCoordAct = actsys.ActorOf(Props.Create<FileWriterCoordinatorActor>(), "fwCoordAct");
            var dbCoordAct = actsys.ActorOf(Props.Create<DbCoordinatorActor>(), "dbCoordAct");
            var wswCoordAct = actsys.ActorOf(Props.Create<WebSiteWatcherCoordinatorActor>(dbCoordAct), "wswCoordAct");

            // website url and content of the landing page to look at
            Dictionary<string, string> dict_url = new Dictionary<string, string>
            {
                { "https://devstack.vwgroup.com/",              "<title>DevStack - Volkswagen AG</title>" },               
                { "https://devstack.vwgroup.com/jira/",         "<title>System Dashboard - DevStack Jira</title>" },
                { "https://devstack.vwgroup.com/confluence/",   "src=\"/confluence/" },
                { "https://devstack.vwgroup.com/bamboo/",       "<title>Log in as a Bamboo user - DevStack Bamboo</title>" },
                { "https://devstack.vwgroup.com/artifactory/",  "<html ng-app=\"artifactory.ui\">" },
                { "https://devstack.vwgroup.com/bitbucket/",    "<title>VW Group Login</title>" }
                
            };


            foreach (var item in dict_url)
            {
                wswCoordAct.Tell(new AddWebSiteWatcherActorMsg(item.Key, item.Value));
            }

            wswCoordAct.Tell(new StartAllWebSiteWatcherActorMsg());






            //wswCoordAct.Tell("test");


            //secondactor.Tell("test");
            //actsys.Stop(wswCoordAct);
            //actsys.Stop(secondactor);
            
            // 1. initialize web site watcher coordinator actor
            // 1.1 initialize file writer corrdinator actor
            // 2. create for each website a web site watcher actor with data sink info csv file
            // 2.2 each web site watching actor 

            Console.ReadLine();

            actsys.Terminate();
        }
    }
}
