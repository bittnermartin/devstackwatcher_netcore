using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DevStackWatcher.DevStackApi.Models;
using DevStackWatcher.DevStackApi.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace DevStackWatcher.DevStackApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            /*
             * Die Konfigurationsinstanz, an die der Abschnitt DevStackDatabaseSettings 
             * der Datei appsettings.json gebunden ist, ist beim 
             * Abhängigkeitsinjektionscontainer (DI, Dependency Injection) registriert. 
             * D. h. die Eigenschaft ConnectionString eines DevStackDatabaseSettings-Objekts 
             * wird beispielsweise mit der Eigenschaft DevStackDatabaseSettings:ConnectionString 
             * in appsettings.json aufgefüllt.
            */
            services.Configure<DevStackDatabaseSettings>(
                Configuration.GetSection(nameof(DevStackDatabaseSettings)));

            /*
             * Die Schnittstelle IDevStackDatabaseSettings ist bei DI mit der 
             * Lebensdauer eines Singletondiensts registriert. Beim Einfügen wird 
             * die Schnittstelleninstanz in ein DevStackDatabaseSettings-Objekt aufgelöst.
             */

            services.AddSingleton<IDevStackDatabaseSettings>(sp =>
                sp.GetRequiredService<IOptions<DevStackDatabaseSettings>>().Value);

            /*
             * Im folgenden Code wird die Klasse HttpResponseService bei DI registriert, 
             * um die Konstruktorinjektion in verarbeitenden Klassen zu unterstützen. 
             * Die Lebensdauer des Singletondiensts ist am besten geeignet, da HttpResponseService 
             * direkt von MongoClient abhängig ist. Gemäß den offiziellen Mongo 
             * Client-Richtlinien zur Wiederverwendung (Mongo Client reuse guidelines) 
             * sollte MongoClient bei DI mit der Lebensdauer eines Singletondiensts 
             * registriert werden.
             */

            services.AddSingleton<HttpResponseService>();

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
