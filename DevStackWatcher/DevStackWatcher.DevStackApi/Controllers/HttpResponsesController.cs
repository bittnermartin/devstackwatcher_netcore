﻿using DevStackWatcher.DevStackApi.Models;
using DevStackWatcher.DevStackApi.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using Serilog;

namespace DevStackWatcher.DevStackApi.Controllers
{
    /*
     * HttpResponsesController verwendet die HttpResponseService-Klasse, 
     * um CRUD-Vorgänge auszuführen.
     * 
     * HttpResponsesController enthält Aktionsmethoden zur Unterstützung 
     * von GET-, POST-, PUT- und DELETE HTTP-Anforderungen.
     * 
     * HttpResponsesController ruft CreatedAtRoute in der Aktionsmethode 
     * Create auf, um eine HTTP 201-Antwort zurückzugeben. Der Statuscode 
     * 201 ist die Standardantwort für eine HTTP POST-Methode, die eine 
     * neue Ressource auf dem Server erstellt. CreatedAtRoute fügt der 
     * Antwort außerdem einen Location-Header hinzu. Der Location-Header 
     * gibt den URI des neu erstellten Buchs an
     */


    [Route("api/[controller]")]
    [ApiController]
    public class HttpResponsesController : ControllerBase
    {
        #region Fields
        /// <summary>
        /// Serilog Logger
        /// </summary>
        private readonly ILogger _log = new LoggerConfiguration().CreateLogger();
        #endregion

        private readonly HttpResponseService _httpResponseService;

        public HttpResponsesController(HttpResponseService httpResponseService)
        {
            _httpResponseService = httpResponseService;
        }

        [HttpGet]
        public ActionResult<List<HttpResponse>> Get() =>
            _httpResponseService.Get();

        
        [HttpGet("{id:length(24)}", Name = "GetHttpResponse")]
        public ActionResult<HttpResponse> Get(string id)
        {
            var httpResponse = _httpResponseService.Get(id);

            if (httpResponse == null)
            {
                return NotFound();
            }

            return httpResponse;
        }
        

        /*
        [HttpGet("{RequestedUrl}", Name = "GetHttpResponsesFromRequestedUrl")]
        public ActionResult<HttpResponse> Get(string requestedUrl)
        {
            var httpResponse = _httpResponseService.Get(requestedUrl);

            if (httpResponse == null)
            {
                return NotFound();
            }

            return httpResponse;
        }
        */

        [HttpPost]
        public ActionResult<HttpResponse> Create(HttpResponse httpResponse)
        {
            _log.Debug("New Data base Entry created for" + httpResponse.RequestedUrl);
            _httpResponseService.Create(httpResponse);

            return CreatedAtRoute("GetHttpResponse", new { id = httpResponse.Id.ToString() }, httpResponse);
        }

        [HttpPut("{id:length(24)}")]
        public IActionResult Update(string id, HttpResponse httpResponseIn)
        {
            var httpResponse = _httpResponseService.Get(id);

            if (httpResponse == null)
            {
                return NotFound();
            }

            _httpResponseService.Update(id, httpResponseIn);

            return NoContent();
        }

        [HttpDelete("{id:length(24)}")]
        public IActionResult Delete(string id)
        {
            var httpResponse = _httpResponseService.Get(id);

            if (httpResponse == null)
            {
                return NotFound();
            }

            _httpResponseService.Remove(httpResponse.Id);

            return NoContent();
        }
    }
}
