﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace DevStackWatcher.DevStackApi.Models
{
    public class HttpResponse
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        [BsonElement("TimeStamp")]
        public DateTime TimeStamp { get; set; }

        public string RequestedUrl { get; set; }

        public string ResponseCode { get; set; }

        public string OverallStatus { get; set; }

        public string RequestDurationMilliSeconds { get; set; }
    }
}
