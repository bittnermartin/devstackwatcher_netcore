﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DevStackWatcher.DevStackApi.Models
{
    /*
     * Mit der vorhergehenden Klasse DevStackDatabaseSettings 
     * werden die Eigenschaftswerte DevStackDatabaseSettings 
     * der Datei appsettings.json gespeichert. Die Namen der 
     * JSON- und der C#-Eigenschaften sind identisch, um den 
     * Zuordnungsprozess zu erleichtern.
     */

    public class DevStackDatabaseSettings : IDevStackDatabaseSettings
    {
        public string HttpResponsesCollectionName { get; set; }
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }

    public interface IDevStackDatabaseSettings
    {
        string HttpResponsesCollectionName { get; set; }
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
    }
}
