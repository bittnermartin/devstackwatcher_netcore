﻿using DevStackWatcher.DevStackApi.Models;
using System.Collections.Generic;
using System.Linq;
using MongoDB.Driver;

namespace DevStackWatcher.DevStackApi.Services
{
    public class HttpResponseService
    {
        private readonly IMongoCollection<HttpResponse> _httpResponses;

        /*
         * Im Code wird eine IDevStackDatabaseSettings-Instanz 
         * mittels Konstruktorinjektion über DI abgerufen. Damit kann auf 
         * die Konfigurationswerte von appsettings.json zugegriffen werden.
         */

        public HttpResponseService(IDevStackDatabaseSettings settings)
        {
            // MongoClient: Liest die Serverinstanz zum Ausführen von Datenbankvorgängen
            var client = new MongoClient(settings.ConnectionString);

            var database = client.GetDatabase(settings.DatabaseName);

            // GetCollection<TDocument>(collection) gibt ein MongoCollection-Objekt 
            // zurück, das die Sammlung darstellt
            _httpResponses = database.GetCollection<HttpResponse>(settings.HttpResponsesCollectionName);
        }

        public List<HttpResponse> Get() =>
            _httpResponses.Find(httpResponse => true).ToList();


        public HttpResponse Get(string id) =>
            _httpResponses.Find<HttpResponse>(httpResponse => httpResponse.Id == id).FirstOrDefault();

        public HttpResponse Create(HttpResponse httpResponse)
        {
            _httpResponses.InsertOne(httpResponse);
            return httpResponse;
        }

        public void Update(string id, HttpResponse httpResponseIn) =>
            _httpResponses.ReplaceOne(httpResponse => httpResponse.Id == id, httpResponseIn);

        public void Remove(HttpResponse httpResponseIn) =>
            _httpResponses.DeleteOne(httpResponse => httpResponse.Id == httpResponseIn.Id);

        public void Remove(string id) =>
            _httpResponses.DeleteOne(httpResponse => httpResponse.Id == id);
    }
}
